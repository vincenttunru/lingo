import {validate} from './validate';

it('should recognise close and correct guesses', () => {
  expect(validate(['r', 'a', 't', 'e', 'l'], ['a', 's', 't', 'e', 'r']))
    .toEqual(['close', 'close', 'correct', 'correct', 'wrong']);
});

it('should not be troubled by repeated occurences of letters of varying correctness', () => {
  expect(validate(['r', 'r', 'a', 'r', 'a'], ['r', 'a', 'a', 'a', 'o']))
    .toEqual(['correct', 'wrong', 'correct', 'wrong', 'close']);
});
