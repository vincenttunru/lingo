import React from 'react';
import { Game } from './components/Game';
import { LongWord } from './components/LongWord';

import './App.scss';

const App: React.FC = () => {
  if (document.location.search === '?mode=long') {
    return <LongWord/>;
  }

  const timeout = (document.location.search === '?mode=auto')
    ? 4
    : -1;

  return (
    <Game timeout={timeout}/>
  );
}

export default App;
